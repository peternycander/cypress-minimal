// https://on.cypress.io/interacting-with-elements

it("Should not crash", () => {
  cy.visit("https://www.cmore.se/artiklar/vanliga-fragor");
  cy.get('[data-test="loader"]').should("not.exist");
});
